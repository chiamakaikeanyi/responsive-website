# Responsive Website
**URL**: [View Website](https://chiamakaikeanyi.gitlab.io/responsive-website/)

### Getting Started
  Navigate to the site’s directory

  ```sh
  cd responsive-website

  open index.html
  ```

A quick look at the top-level files and directories

    .
    ├── css
    ├── img
    ├── js
    ├── .gitlab-ci.yml
    ├── index.de.html
    ├── index.html
    └── README.md

### Progress on Bonus Challenge

- [x] Adjust the height and spacing of the first section so that the second section is always a little visible - attracting the user to scroll
- [x] Make it "mobile first"
- [ ] Make it "no-js first"
- [x] Make it accessible
- [x] Translate it into two languages
  - [x] Preferably including a right-to-left language