var toggleButton = document.querySelector(".navigation-drawer__toggle");
var navigationDrawer = document.querySelector(".navigation-drawer");

toggleButton.addEventListener("click", function (event) {
  event.preventDefault();

  navigationDrawer.classList.toggle("navigation-drawer__open");
});
